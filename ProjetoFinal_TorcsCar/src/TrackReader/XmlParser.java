/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrackReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Classe que altera o xml original do mapa. 
 * Aqui é lido o xml com as informações do mapa e verificado se há bocados de 
 * texto que não são aceites nas bibliotecas que fazem a leitura do xml. Retira 
 * estes bocados de texto, espaços desnecessários e guarda um novo xml com a 
 * informação válida.
 * @author Raphael Carvalho 64044
 */
public class XmlParser {
    
    /**
     * Função que elimina bocados de texto que não são válidos nas bibliotecas 
     * do xml.
     * @param filename Nome do ficheiro original
     * @return
     */
    public static String xmlParser(String filename){
        String parsedFilename = "parsed_"+filename;
        try{
            FileReader fr = new FileReader(filename);
            BufferedReader br = new BufferedReader(fr);
            String currentLine;
            String fileContent = "";
            boolean isComment = false;
            while((currentLine = br.readLine()) != null){
                boolean errorLine = false;
                if(currentLine.replaceAll("\\s+","").startsWith("<!--")){
                    isComment = true;
                }
                if(currentLine.replaceAll("\\s+","").startsWith("<!") && !isComment){
                    errorLine = true;
                }
                if(!currentLine.replaceAll("\\s+","").startsWith("<") && !isComment){
                    errorLine = true;
                }
                    
                
                if(!isComment && !errorLine){
                    
                    fileContent += currentLine.replaceAll("&.*;", "") + "\n";
                }
                if(currentLine.replaceAll("\\s+","").endsWith("-->")){
                    isComment = false;
                }
            }
            //System.out.println(fileContent);
            PrintWriter pw = new PrintWriter("parsed_" + filename);
            pw.print(fileContent);
            pw.close();
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println("Error reading the file: " + filename);
        }
        return parsedFilename;
    }
}
