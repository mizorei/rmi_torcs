/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrackReader;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Raphael Carvalho 64044
 */
public class TrackReader {
    private File xmlFile;
    private Track track;

    /**
     * Cria um objecto TrackReader e faz a leitura dos parâmetros de interesse
     * @param xmlPath Caminho ou nome do ficheiro que contém o xml a ser lido.
     */
    public TrackReader(String xmlPath){
        xmlFile = new File(xmlPath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        
        try{
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            
            doc.getDocumentElement().normalize();
            //System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            
            Node mainTrack = getMainTrack(doc);
            Node trackSegments = getTrackSegment(mainTrack);
            double width = getWidth(mainTrack);
            String surfaceName = getSurface(mainTrack);
            Node surfaces = getSurfaces(doc);
            double friction = getFriction(surfaces, surfaceName);
            double stepslg = getStepsLength(mainTrack);
            track = new Track(width, friction, stepslg);
            readSegments(trackSegments);
            track.redefineSegments();
            track.createMap();
            track.defineMaxSpeeds();
            track.defineTrackSide2();
            
        }catch(ParserConfigurationException e){
            System.out.println("Erro na criação do dBuilder");
        } catch (SAXException ex) {
            Logger.getLogger(TrackReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("Erro na leitura do xml");
        }
    }

    private Node getMainTrack(Document doc){
        NodeList nList = doc.getElementsByTagName("section");
        for(int i = 0; i < nList.getLength(); i++){
            Node node = nList.item(i);
            NamedNodeMap attributes = node.getAttributes();
            Node attr = attributes.getNamedItem("name");
            if(attr != null){
                if(attr.getTextContent().equals("Main Track")){
                    return node;                    
                }
            }
        }
        return null;
    }
    
    private Node getSurfaces(Document doc){
        NodeList nList = doc.getElementsByTagName("section");
        for(int i = 0; i < nList.getLength(); i++){
            Node node = nList.item(i);
            NamedNodeMap attributes = node.getAttributes();
            Node attr = attributes.getNamedItem("name");
            if(attr != null){
                if(attr.getTextContent().equals("Surfaces")){
                    return node;                    
                }
            }
        }
        return null;
    }
    
    private Node getTrackSegment(Node nodeMT){
        NodeList nodeList = nodeMT.getChildNodes();
        for(int j = 0; j< nodeList.getLength() ; j++){
            Node node = nodeList.item(j);
            if(node.getNodeName().compareTo("section") == 0){
                NamedNodeMap attributes = node.getAttributes();
                Node attr = attributes.getNamedItem("name");
                if(attr != null){
                    if(attr.getTextContent().equals("Track Segments")){
                        return node;
                    }
                }
            }
        }
        return null;
    }
    
    private double getWidth(Node nodeMT){
        NodeList nodeList = nodeMT.getChildNodes();
        for(int j = 0; j< nodeList.getLength() ; j++){
            Node node = nodeList.item(j);
            if(node.getNodeName().compareTo("attnum") == 0){
                NamedNodeMap attributes = node.getAttributes();
                Node attr = attributes.getNamedItem("name");
                if(attr != null){
                    if(attr.getTextContent().equals("width")){
                        attr = attributes.getNamedItem("val");
                        if(attr != null){
                            double val= Double.parseDouble(attr.getTextContent());
                            return val;
                        }
                    }
                }
            }
        }
        return 0;
    }
    
    private double getStepsLength(Node nodeMT){
        NodeList nodeList = nodeMT.getChildNodes();
        for(int j = 0; j< nodeList.getLength() ; j++){
            Node node = nodeList.item(j);
            if(node.getNodeName().compareTo("attnum") == 0){
                NamedNodeMap attributes = node.getAttributes();
                Node attr = attributes.getNamedItem("name");
                if(attr != null){
                    if(attr.getTextContent().equals("profil steps length")){
                        attr = attributes.getNamedItem("val");
                        if(attr != null){
                            double val= Double.parseDouble(attr.getTextContent());
                            return val;
                        }
                    }
                }
            }
        }
        return 0;
    }
    
    private String getSurface(Node nodeMT){
        NodeList nodeList = nodeMT.getChildNodes();
        for(int j = 0; j< nodeList.getLength() ; j++){
            Node node = nodeList.item(j);
            if(node.getNodeName().compareTo("attstr") == 0){
                NamedNodeMap attributes = node.getAttributes();
                Node attr = attributes.getNamedItem("name");
                if(attr != null){
                    if(attr.getTextContent().equals("surface")){
                        attr = attributes.getNamedItem("val");
                        if(attr != null){
                            String val= attr.getTextContent();
                            return val;
                        }
                    }
                }
            }
        }
        return "";
    }

    private void readSegments(Node nodeTS){
        NodeList nodeList = nodeTS.getChildNodes();
        for(int i = 0; i<nodeList.getLength(); i++){
            Node node = nodeList.item(i);
            if(node.getNodeName().compareTo("section") == 0){
                readSegment(node);
            }
        }
    }
    
    private void readSegment(Node nodeSeg){
        NodeList nodeList = nodeSeg.getChildNodes();
        SegmentType segType = null;
        double comp = 0, arc = 0, radius = 0, radiusEnd = 0, banking = 0, steps = 1;
        String profil = "spline";
        for(int i = 0; i<nodeList.getLength(); i++){
            Node node = nodeList.item(i);
            
            if(node.getNodeName().equals("attstr") || node.getNodeName().equals("attnum")){
                NamedNodeMap attributes = node.getAttributes();
                Node attr = attributes.getNamedItem("name");
                if(attr != null){
                    if(attr.getTextContent().equals("type")){
                        Node type = attributes.getNamedItem("val");
                        if(type != null){
                            if(type.getTextContent().equals("str")){
                                segType = SegmentType.RECTA;
                            }else if(type.getTextContent().equals("rgt")){
                                segType = SegmentType.CURVA_DIR;
                            }else if(type.getTextContent().equals("lft")){
                                segType = SegmentType.CURVA_ESQ;
                            }
                        }
                    }else if(attr.getTextContent().equals("lg")){
                        attr = attributes.getNamedItem("unit");
                        if(attr != null){
                            String unit = attr.getTextContent();
                            attr = attributes.getNamedItem("val");
                            if(attr != null){
                                comp = Double.parseDouble(attr.getTextContent());
                                if(unit.equals("ft")){
                                    comp *= 0.3048;
                                }
                            }
                        }
                    }else if(attr.getTextContent().equals("arc")){
                        attr = attributes.getNamedItem("unit");
                        if(attr != null){
                            String unit = attr.getTextContent();
                            attr = attributes.getNamedItem("val");
                            if(attr != null){
                                arc = Double.parseDouble(attr.getTextContent());
                                if(unit.equals("deg")){
                                    arc = Math.toRadians(arc);
                                }
                            }
                        }
                    }else if(attr.getTextContent().equals("radius")){
                        attr = attributes.getNamedItem("unit");
                        if(attr != null){
                            String unit = attr.getTextContent();
                            attr = attributes.getNamedItem("val");
                            if(attr != null){
                                radius = Double.parseDouble(attr.getTextContent());
                                if(unit.equals("ft")){
                                    radius *= 0.3048;
                                }
                            }
                        }
                    }else if(attr.getTextContent().equals("end radius")){
                        attr = attributes.getNamedItem("unit");
                        if(attr != null){
                            String unit = attr.getTextContent();
                            attr = attributes.getNamedItem("val");
                            if(attr != null){
                                radiusEnd = Double.parseDouble(attr.getTextContent());
                                if(unit.equals("ft")){
                                    radiusEnd *= 0.3048;
                                }
                            }
                        }
                    }else if(attr.getTextContent().equals("banking end")){
                        attr = attributes.getNamedItem("unit");
                        if(attr != null){
                            String unit = attr.getTextContent();
                            attr = attributes.getNamedItem("val");
                            if(attr != null){
                                banking = Double.parseDouble(attr.getTextContent());
                                if(unit.equals("deg")){
                                    banking = Math.toRadians(banking);
                                }
                            }
                        }
                    }else if(attr.getTextContent().equals("profil")){
                        attr = attributes.getNamedItem("val");
                        if(attr != null){
                            profil = attr.getTextContent();
                        }
                    }else if(attr.getTextContent().equals("profil steps")){
                        attr = attributes.getNamedItem("val");
                        if(attr != null){
                            steps = Double.parseDouble(attr.getTextContent());
                        }
                    }
                }
            }
            
        }
        Segment seg = new Segment(segType, (long)comp, arc, radius, radiusEnd, banking, profil, steps);
        track.addSegment(seg);
    }
    
    private double getFriction(Node nodeSur, String surfaceName){
        NodeList nodeList = nodeSur.getChildNodes();
        for(int j = 0; j< nodeList.getLength() ; j++){
            Node node = nodeList.item(j);
            if(node.getNodeName().compareTo("section") == 0){
                NamedNodeMap attributes = node.getAttributes();
                Node attr = attributes.getNamedItem("name");
                if(attr != null){
                    if(attr.getTextContent().equals(surfaceName)){
                        nodeList = node.getChildNodes();
                        for(int i = 0; i< nodeList.getLength(); i++){
                            node = nodeList.item(i);
                            if(node.getNodeName().compareTo("attnum") == 0){
                                attributes = node.getAttributes();
                                attr = attributes.getNamedItem("name");
                                if(attr != null){
                                    if(attr.getTextContent().equals("friction")){
                                        attr = attributes.getNamedItem("val");
                                        if(attr != null){
                                            return Double.parseDouble(attr.getTextContent());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return 0;
    }
    
    /**
     * Função para obter informações do track depois de lido. Contém informações 
     * do segmentos e valores de interesse da pista.
     * @return Objecto track que contém as informações da pista
     */
    public Track getTrack(){
        return track;
    }
}
