/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrackReader;

/**
 *
 * @author Raphael Carvalho 64044
 */
public enum SegmentType {
    RECTA, CURVA_ESQ, CURVA_DIR
}
