/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrackReader;

/**
 *
 * @author Raphael Carvalho 64044
 */
public class Segment {
    private SegmentType segmentType;
    private double comprimento, arcoRad, raio, raioFim, banking, steps;
    private String profil;
    
    public Segment(SegmentType sT, double comp, double rad, double radius, double endRadius, double banking, String profil, double steps ){
        segmentType = sT;
        arcoRad = 0;
        raio = 0;
        raioFim = 0;
        comprimento = 0;
        this.profil = profil;
        this.steps = steps;
        if(segmentType == SegmentType.RECTA){
            comprimento = comp;
        }else{
            arcoRad = rad;
            raio = radius;
            if(endRadius == 0){
                raioFim = radius;
            }else{
                raioFim = endRadius;
            }
            comprimento = ((raio + raioFim) / 2) * arcoRad;
            /*if(arcoRad <= 0.26){
                segmentType = SegmentType.RECTA;
            }*/
            this.banking = banking;
            if(segmentType == SegmentType.CURVA_ESQ){
                this.banking *= -1;
            }
        }
    }
    
    public SegmentType getSegmentType(){
        return segmentType;
    }
    
    public double getComprimento() {
        return comprimento;
    }

    public double getArcoRad() {
        return arcoRad;
    }

    public double getRaio() {
        return raio;
    }

    public double getRaioFim() {
        return raioFim;
    }

    public double getBanking() {
        return banking;
    }

    public double getSteps() {
        return steps;
    }

    public String getProfil() {
        return profil;
    }
    
    
}
