/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrackReader;

import java.util.ArrayList;

/**
 *
 * @author Raphael Carvalho 64044
 */
public class Track {
    private double largura, stepslg;
    private ArrayList<Segment> segmentList;
    
    private MapPart[] map;
    private double segmentLenght;
    
    private final double frictionCoef;
    
    public Track(double largura, double frictionCoef, double stepslg){
        this.largura = largura;
        this.frictionCoef = frictionCoef;
        this.stepslg = stepslg;
        segmentList = new ArrayList<>();
        segmentLenght = 0;
    }
    
    public void addSegment(Segment segment){
        segmentList.add(segment);
        segmentLenght += segment.getComprimento();
    }
    
    public void redefineSegments(){
        ArrayList<Segment> newSegmentList = new ArrayList<>();
        segmentLenght = 0;
        
        for(Segment seg: segmentList){
            double steps;
            if(seg.getProfil().equals("spline")){
                steps = seg.getSteps();
                if(steps == 1){
                    if(stepslg != 0){
                        steps = (int)(seg.getComprimento() / stepslg) + 1;
                    } else {
                        steps = 1;
                    }
                }
            }else{
                steps = 1;
            }
            
            double curStep = 0;
            double curArc = seg.getArcoRad() / steps;
            double curLength = seg.getComprimento() / steps;
            double dradius = (seg.getRaioFim() - seg.getRaio()) / steps;
            if(seg.getRaio() != seg.getRaioFim()){
                if(steps!=1){
                    dradius = (seg.getRaioFim() - seg.getRaio())/(steps-1);
                    double tmpAngle = 0;
                    double tmpRadius = seg.getRaio();
                    for(curStep = 0; curStep < steps; curStep++){
                        tmpAngle += curLength / tmpRadius;
                        tmpRadius += dradius;
                    }
                    curLength *= seg.getArcoRad() / tmpAngle;
                }
            }
            curStep = 0;
            double radius = seg.getRaio();
            while(curStep < steps){
                if(dradius != 0){
                    curArc = curLength / radius;
                }
                Segment newSegment = new Segment(seg.getSegmentType(), curLength, curArc, radius, 0, seg.getBanking(), seg.getProfil(), 1);
                newSegmentList.add(newSegment);
                curStep++;
                if(seg.getSegmentType() != SegmentType.RECTA){
                    radius += dradius;
                }
            }
        }
        segmentList = new ArrayList<>();
        for(Segment seg: newSegmentList){
            addSegment(seg);
        }
    }
    
    public void createMap(){
        map = new MapPart[(int)segmentLenght];
        int i = 0;
        double total = 0;
        Segment lastSeg = segmentList.get(segmentList.size()-1);
        for(int j = 0; j<segmentList.size(); j++){
            Segment seg = segmentList.get(j);
            total += seg.getComprimento();
            while(i< (int)total){
                double maxSpeed;// Em m/s
                double side;
                if(seg.getSegmentType() != SegmentType.RECTA){
                    double banking = seg.getBanking();
                    if(Math.abs(lastSeg.getBanking()) > Math.abs(banking)){
                        banking = lastSeg.getBanking();
                    }
                    lastSeg = seg;
                    double arc = 0.0;
                    int k = j;
                    if(k>=segmentList.size()){
                        k=0;
                    }
                    while(segmentList.get(k).getSegmentType() == seg.getSegmentType() && arc < Math.PI/2.0){
                        arc += segmentList.get(k).getArcoRad();
                        k++;
                        if(k>=segmentList.size()){
                            k=0;
                        }
                    }
                    arc /= (Math.PI / 2.0);
                    
                    double raio = seg.getRaio() / Math.sqrt(arc);
                    
                    maxSpeed = findMaxSpeedTurn(raio, banking);
                    if(seg.getSegmentType() == SegmentType.CURVA_DIR){
                        side = -0.8;
                    }else{
                        side = 0.8;
                    }
                }else{
                    maxSpeed = 120;
                    side = 0;
                }
                double arcTotal = 0;
                double lengthTotal = 0;
                
                
                int k = j;
                while( segmentList.get(k).getSegmentType() == seg.getSegmentType()){
                    arcTotal += segmentList.get(k).getArcoRad();
                    lengthTotal += segmentList.get(k).getComprimento();
                    k++;
                    if(k>= segmentList.size()){
                        k=0;
                    }
                }
                k = j-1;
                if(k<0){
                    k=segmentList.size()-1;
                }
                
                while(segmentList.get(k).getSegmentType() == seg.getSegmentType()){
                    arcTotal += segmentList.get(k).getArcoRad();
                    lengthTotal += segmentList.get(k).getComprimento();                    
                    k--;
                    if(k<0){
                        k=segmentList.size()-1;
                    }
                }
                MapPart mapPart = new MapPart(seg.getSegmentType(), maxSpeed*3.6, side, seg.getRaio(), seg.getArcoRad()/seg.getComprimento(), arcTotal, lengthTotal);
                
                map[i] = mapPart;
                i++;
            }
        }
    }
    
    public void defineMaxSpeeds(){
        for(int i = 0; i<map.length ; i++){
            if(map[i].getPartType() != SegmentType.RECTA){
                double maxSpeedFinal = map[i].getMaxSpeed() / 3.6;
                int j = i-1;
                boolean update = true;
                if(j<0){
                    j = map.length-1;
                }
                do{
                    int temp = j;
                    if(j<0){
                        j = map.length-1;
                    }
                    double speed = findMaxSpeedBeforeTurn(maxSpeedFinal, i-temp);
                    if(speed <= map[j].getMaxSpeed() / 3.6){
                        map[j].setMaxSpeed(speed*3.6);
                    }else{
                        update = false;
                    }
                    j--;
                }while(update);
            }
        }
    }
    
    public void defineTrackSide(){
        for(int i = 0; i<map.length; i++){
            if(map[i].getPartType() != SegmentType.RECTA){
                double side;
                double sideWide = 1.6;
                if(map[i].getPartType() == SegmentType.CURVA_DIR){
                    side=-1*sideWide/2;
                }else{
                    side=sideWide/2;
                }
                map[i].setTrackSide(side);
                int j = i-1;
                if(j<0){
                    j = map.length-1;
                }
                int last = j;
                int first = j;
                
                while(map[j].getPartType() == SegmentType.RECTA){
                    first = j;
                    j--;
                    if(j<0){
                        j = map.length-1;
                    }
                }
                int dist;
                if(first > last){
                    dist = last-(map.length-first);
                }else{
                    dist = last-first;
                }
                if(dist!=0){
                    double sidePart = sideWide/80;
                    int sideLastFactor = getSideFactor(i);
                    int sideFirstFactor = getSideFactor(j);

                    double sideLast = map[i].getTrackSide();
                    double sideFirst = map[j].getTrackSide();
                    int k;
                    int tempFirst = first, tempLast = last;
                    for(k = 0; k<80 && k < dist/2; k++){
                        sideFirst += sideFirstFactor * sidePart;
                        sideLast += sideLastFactor * sidePart;
                        if(sideFirst > 0.8){
                            sideFirst = 0.8;
                        }else if(sideFirst < -0.8){
                            sideFirst = -0.8;
                        }
                        if(sideLast > 0.8){
                            sideLast = 0.8;
                        }else if(sideLast < -0.8){
                            sideLast = -0.8;
                        }
                        tempFirst = first+k;
                        if(tempFirst>=map.length){
                            tempFirst=0;
                        }
                        tempLast = last-k;
                        if(tempLast < 0){
                            tempLast = map.length-1;
                        }
                        map[tempFirst].setTrackSide(sideFirst);
                        map[tempLast].setTrackSide(sideLast);
                    }
                    if(k==80){
                        if(tempFirst > tempLast){
                            dist = tempLast-(map.length-tempFirst);
                        }else{
                            dist = tempLast-tempFirst;
                        }
                        if(dist!=0){
                            sideLast = map[tempLast].getTrackSide();
                            sideFirst = map[tempFirst].getTrackSide();
                            sidePart=(sideLast - sideFirst) / dist;
                            for(int o = tempFirst; o <= tempLast; o++){
                                sideFirst += sidePart;
                                map[o].setTrackSide(sideFirst);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void defineTrackSide2(){
        double error = 0.03;
        for(int i = 0; i<map.length; i++){
            if(map[i].getPartType() != SegmentType.RECTA){
                double sideWide = 1.6;
                map[i].setTrackSide(0);
                
                // Antes da curva
                int j = i-1;
                if(j<0){
                    j = map.length-1;
                }
                int last = j;
                int first = j;
                
                while(map[j].getPartType() == SegmentType.RECTA){
                    first = j;
                    j--;
                    if(j<0){
                        j = map.length-1;
                    }
                }
                
                // Entre curvas
                int dist;
                if(first == last)
                    dist = 0;
                else
                    dist = last-first+1;
                
                if(dist!=0){
                    // Distancia para preparar curva
                    double speedLast = map[i].getMaxSpeed();
                    double speedFirst = map[j].getMaxSpeed();
                    
                    if(speedLast>200){
                        speedLast=200;
                    }
                    if(speedFirst>200){
                        speedFirst=200;
                    }
                    
                    double sideLastLength = ((speedLast/3.6)/map[i].getArcTurnTotal())*2;
                    double sideFirstLength = ((speedFirst/3.6)/map[j].getArcTurnTotal())*2;
                    // Valor a atualizar no side
                    double sidePartLast = (sideWide / 2) / sideLastLength;
                    double sidePartFirst = (sideWide / 2) / sideFirstLength;
                    
                    int sideLastFactor = getSideFactor(i);
                    int sideFirstFactor = getSideFactor(j);

                    double side = 0.0;
                    
                    // Segmento de recta separa duas curvas iguais
                    if(sideLastFactor == sideFirstFactor){
                        int k;
                        int tempFirst = first, tempLast = last;
                        for(k=0; k<dist; k++){
                            side -= sideFirstFactor * sidePartFirst;
                            if(side > sideWide/2){
                                side = sideWide/2;
                            }else if(side < -sideWide/2){
                                side = -sideWide/2;
                            }
                            map[tempFirst].setTrackSide(side);
                            tempFirst++;
                            if(tempFirst>=map.length){
                                tempFirst=0;
                            }
                        }
                        side = 0.0;
                        for(k=0; k<dist; k++){
                            side -= sideLastFactor * sidePartLast;
                            if(side > sideWide/2){
                                side = sideWide/2;
                            }else if(side < -sideWide/2){
                                side = -sideWide/2;
                            }
                            if(map[tempLast].getTrackSide() < side+error && map[tempLast].getTrackSide() > side-error){
                                break;
                            }
                            map[tempLast].setTrackSide(side);
                            tempLast--;
                            if(tempLast < 0){
                                tempLast = map.length-1;
                            }
                            
                        }
                    }else{ // Segmento de recta que separa duas curvas opostas                      
                        int temp = (int)(dist*0.25);
                        int tempFirst = first, tempLast = last;
                        int k;
                        double sideFirst = 0.0, sideLast = 0.0;
                        for(k=0; k<temp; k++){
                            sideFirst -= sideFirstFactor * sidePartFirst;
                            if(sideFirst > sideWide/2){
                                sideFirst = sideWide/2;
                            }else if(sideFirst < -sideWide/2){
                                sideFirst = -sideWide/2;
                            }
                            map[tempFirst].setTrackSide(sideFirst);
                            tempFirst++;
                            if(tempFirst>=map.length){
                                tempFirst=0;
                            }
                            
                            sideLast -= sideLastFactor * sidePartLast;
                            if(sideLast > sideWide/2){
                                sideLast = sideWide/2;
                            }else if(sideLast < -sideWide/2){
                                sideLast = -sideWide/2;
                            }
                            map[tempLast].setTrackSide(sideLast);
                            tempLast--;
                            if(tempLast < 0){
                                tempLast = map.length-1;
                            }
                        }
                        tempFirst = first+temp;
                        temp = dist-temp*2;
                        double diff = sideLast-sideFirst;
                        double diffPart = diff/temp;
                        for(k=0; k<temp; k++){
                            sideFirst += diffPart;
                            if(sideFirst > sideWide/2){
                                sideFirst = sideWide/2;
                            }else if(sideFirst < -sideWide/2){
                                sideFirst = -sideWide/2;
                            }
                            
                            map[tempFirst].setTrackSide(sideFirst);
                            if(temp < 50){
                                map[tempFirst].setTrackSide(sideLast);
                            }
                            tempFirst++;
                            if(tempFirst>=map.length){
                                tempFirst=0;
                            }
                        }
                        
                        /*for(k=0; k<temp; k++){
                            sideLast += sideLastFactor * diffPart;
                            if(sideLast > sideWide/2){
                                sideLast = sideWide/2;
                            }else if(sideLast < -sideWide/2){
                                sideLast = -sideWide/2;
                            }
                            if(map[tempLast].getTrackSide() < side+error && map[tempLast].getTrackSide() > side-error){
                                break;
                            }
                            map[tempLast].setTrackSide(side);
                            tempLast--;
                            if(tempLast < 0){
                                tempLast = map.length-1;
                            }
                            
                        }*/
                    }
                }
                
                // Na curva
                double arcPart = Math.toRadians(5);
                int k;
                
                int firstPart = i;
                int lastPart = i;
                while(map[firstPart].getPartType() == map[lastPart].getPartType()){
                    lastPart++;
                    
                    if(lastPart>=map.length){
                        lastPart = 0;
                    }
                }
                if(lastPart==0){
                    lastPart = map.length-1;
                }else{
                    lastPart--;
                }
                int curvDist;
                if(firstPart > lastPart){
                    curvDist = lastPart-(map.length-firstPart) +1;
                }else{
                    curvDist = lastPart-firstPart+1;
                }
                int temp;
                double firstLength=0, lastLength=0;
                double firstArc=0, lastArc=0;
                temp = firstPart;
                while(temp<=lastPart){
                    firstArc += map[temp].getArcSegment();
                    firstLength += 1.0;
                    if(firstArc >= arcPart){
                        break;
                    }
                    temp++;
                    if(temp > map.length-1){
                        temp = 0;
                    }
                }
                temp = lastPart;
                while(temp>=firstPart){
                    lastArc += map[temp].getArcSegment();
                    lastLength +=1.0;
                    if(lastArc >= arcPart){
                        break;
                    }
                    temp--;
                    if(temp<0){
                        temp = map.length-1;
                    }
                }
                if(firstArc < arcPart || lastArc < arcPart){
                    firstLength = (firstLength * arcPart) / firstArc;
                    lastLength = (lastLength * arcPart) / lastArc;
                }
                double sidePartFirst = (sideWide / 2) / firstLength;
                double sidePartLast = (sideWide / 2) / lastLength;
                int factor = getSideFactor(firstPart);
                double side = 0.0;
                temp = firstPart;
                for(k=0; k<curvDist; k++){
                    side += factor * sidePartFirst;
                    if(side > sideWide/2){
                        side = sideWide/2;
                    }else if(side < -sideWide/2){
                        side = -sideWide/2;
                    }
                    map[temp].setTrackSide(side);
                    temp++;
                    if(temp>=map.length){
                        temp=0;
                    }
                }
                temp = lastPart;
                side = 0.0;
                for(k=0; k<curvDist; k++){
                    side += factor * sidePartLast;
                    if(side > sideWide/2){
                        side = sideWide/2;
                    }else if(side < -sideWide/2){
                        side = -sideWide/2;
                    }
                    if(map[temp].getTrackSide() < side+error && map[temp].getTrackSide() > side-error){
                        break;
                    }
                    map[temp].setTrackSide(side);
                    temp--;
                    if(temp < 0){
                        temp = map.length-1;
                    }
                }
                i=lastPart;
            }
        }
    }
    
    private int getSideFactor(int num){
        if(map[num].getPartType() == SegmentType.CURVA_ESQ){
            return 1;
        }else{
            return -1;
        }
    }
    
    public int listSize(){
        return segmentList.size();
    }
    
    public Segment getSegment(int i){
        return segmentList.get(i);
    }
    
    public void printMap(){
        for(int i = 0; i<map.length; i++){
            //System.out.println("i: " + i + " part: " + map[i].getPartType() + " maxspeed: " + map[i].getMaxSpeed());
            //System.out.println("i: " + i + " arc: " + map[i].getArcSegment() + " arcTotal: " + map[i].getArcTurnTotal());
            System.out.println("i: " + i + " Segment: " + map[i].getPartType() + " side: " + map[i].getTrackSide() );
        }
    }
    
    public void printLength(){
        double length = 0.0;
        for(Segment seg : segmentList){
            length += seg.getComprimento();
        }
        System.out.println("Length: " + length);
    }
    public MapPart[] getMap(){
        return map;
    }
    
    private double findMaxSpeedTurn(double raio, double banking){
        double mu = (Math.sin(banking) + frictionCoef * Math.cos(banking))
                /(Math.cos(banking)-frictionCoef * Math.sin(banking));
        double CA = initCA();
        double mass = 1150;
        double temp = 1.0 - Math.min(1.0, raio * CA * mu / mass);
        if(temp==0){
            temp = 1;
        }
        return Math.sqrt((raio * 9.8 * mu) / temp);
    }
    
    private double initCA(){
        double rearwingarea = 0.7;
        double rearwingangle = 14;
        double wingca = 1.23 * rearwingarea * Math.sin(rearwingangle);
        
        double cl = 0.69 + 0.7;
        
        double h = 4 * 0.09 * 1.5;
        
        h = h*h;
        h = h*h;
        h = 2.0 * Math.exp(-3.0*h);
        return h * cl + 4.0*wingca;
    }
    
    private double findMaxSpeedBeforeTurn(double speedFinal, int distance){
        return Math.sqrt(Math.pow(speedFinal, 2) + 2*frictionCoef*9.8*distance);
    }
}
