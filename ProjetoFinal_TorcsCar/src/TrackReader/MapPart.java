/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TrackReader;

/**
 *
 * @author Raphael Carvalho 64044
 */
public class MapPart {
    private SegmentType partType;
    private double maxSpeed;
    private double trackSide;
    private double arcSegment; // arc do metro do segmento
    private double arcTurnTotal;
    private double radius;
    private double lengthTotal;
    
    public MapPart(SegmentType partType, double maxSpeed, double side, double radius, double arcSegment, double arcTurnTotal, double lengthTotal){
        this.partType = partType;
        this.maxSpeed = maxSpeed;
        this.trackSide = side;
        this.arcSegment = arcSegment;
        this.arcTurnTotal = arcTurnTotal;
        this.radius = radius;
        this.lengthTotal = lengthTotal;
    }
    
    public SegmentType getPartType() {
        return partType;
    }

    public void setMaxSpeed(double maxSpeed) {
        if(maxSpeed>400){
            maxSpeed=400;
        }
        this.maxSpeed = maxSpeed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setTrackSide(double trackSide) {
        this.trackSide = trackSide;
    }

    public double getTrackSide() {
        return trackSide;
    }
    
    public double getArcSegment() {
        return arcSegment;
    }

    public double getArcTurnTotal() {
        return arcTurnTotal;
    }

    public double getRadius() {
        return radius;
    }

    public double getLengthTotal() {
        return lengthTotal;
    }
}
