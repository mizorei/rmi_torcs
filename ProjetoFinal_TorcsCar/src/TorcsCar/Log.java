/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TorcsCar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raphael Carvalho 64044
 */
public class Log {
    private static String filename = "log_timeLap.txt";
    private static final String version = "8";
    private static final String info = "Suavizar a posição do carro se " +
            "side tende para ele.  ";
    
    public static boolean checkIfFileExists(String track){
        //String[] strs = track.split(".");
        File f = new File(filename);
        return f.exists() && !f.isDirectory();
    }
    
    public static void createLogFile(String track){
        //String[] strs = track.split(".");
        //filename = strs[0] + filename;
        File file = new File(filename);
        try {
            PrintWriter writer = new PrintWriter(file);
            writer.println("----------------------------------------");
            writer.println("-------- TORCS SIMULATOR RESULTS -------");
            writer.println("----------------------------------------");
            writer.println("-- Work Done By:          --------------");
            writer.println("-- Raphael Carvalho 64044 --------------");
            writer.println("-- Bruno Teixeira 65739   --------------");
            writer.println("----------------------------------------");
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void addNewRace(String track){
        File file = new File(filename);
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(file, true));
            writer.println("----------------------------------------");
            writer.println("----------------------------------------");
            writer.println("-- Version: " + version);
            String[] strs = info.split(" ");
            int sizeLine = 40;
            writer.print("-- Info: ");
            int temp = 9;
            for(int i = 0; i<strs.length; i++){
                if(temp + strs[i].length() > sizeLine){
                    writer.println();
                    temp=0;
                }
                writer.print(strs[i] + " ");
                temp += strs[i].length() + 1;
            }
            writer.println();
            writer.println("-- Track: " + track);
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            writer.println("-- Date: " + dateFormat.format(date));
            writer.println("");
            writer.printf("  %-7s -   %-10s", "Num", "Time");
            writer.println("");
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void addLap(int num, double time){
        File file = new File(filename);
        try{
            PrintWriter writer = new PrintWriter(new FileWriter(file, true));
            writer.printf("  %-7d -   %-10f", num, time);
            writer.println("");
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Log.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
